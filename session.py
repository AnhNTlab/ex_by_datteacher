import pickle
from http.cookies import SimpleCookie

import null as null
from requests_html import HTMLSession

session = HTMLSession()

userName = 'test'
passWord = 'test'

try:
    with open('data.txt', 'rb') as f:
        cookieSave = session.cookies.update(pickle.load(f))
        req = session.post(
            'http://testphp.vulnweb.com/userinfo.php',
            data=cookieSave,
            headers={
                'Content-Type': 'application/x-www-form-urlencoded',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 '
                              '(KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
                'Host': 'testphp.vulnweb.com',
                'Origin': 'http://testphp.vulnweb.com',
                'Referer': 'http://testphp.vulnweb.com/login.php',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/'
                          'webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'en-US,en;q=0.9,vi;q=0.8'
            }
        )
    print(1)
    print(req.html.html)
except:
    print(2)
    req = session.post(
        'http://testphp.vulnweb.com/userinfo.php',
        data={
            'uname': userName,
            'pass': passWord
        },
        headers={
            'Content-Type': 'application/x-www-form-urlencoded',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 '
                          '(KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
            'Host': 'testphp.vulnweb.com',
            'Origin': 'http://testphp.vulnweb.com',
            'Referer': 'http://testphp.vulnweb.com/login.php',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/'
                      'webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'en-US,en;q=0.9,vi;q=0.8'
        }
    )
    cookie = SimpleCookie()
    cookie.load(req.cookies)

    if req.cookies != null:
        a_file = open("data.txt", "wb")
        pickle.dump(req.cookies, a_file)
        a_file.close()
